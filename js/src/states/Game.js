
/*  Main game state of game  */

TanksGameWrapper.Game = function (game) {

  this.tank = null;
  this.turret = null;
  this.flame = null;
  this.bullet = null;

  this.background = null;
  this.targets = null;

  this.power = 300;
  this.powerText = null;

  this.cursors = null;
  this.fireButton = null;

};

TanksGameWrapper.Game.prototype.preload = function () {};
TanksGameWrapper.Game.prototype.create = function () {

  // add background
  this.background = this.add.sprite(0, 0, "background");

  //  TARGETS
  //                            parent, name, addToStage, enableBody, physicsBodyType
  this.targets = this.add.group(this.game.world, "targets", false, true, Phaser.Physics.ARCADE);

  this.targets.create(300, 390, "target");
  this.targets.create(500, 390, "target");
  this.targets.create(700, 390, "target");
  this.targets.create(900, 390, "target");

  this.targets.setAll("body.allowGravity", false);

  // BULLET
  this.bullet = this.add.sprite(0, 0, "bullet");
  this.bullet.exists = false;
  this.physics.arcade.enable(this.bullet);

  // TANK
  this.tank = this.add.sprite(24, 383, "tank");

  //  TURRET
  this.turret = this.add.sprite(this.tank.x+30, this.tank.y+14, "turret");

  // FLAME
  this.flame = this.add.sprite(0, 0, "flame");
  this.flame.anchor.set(0.5);
  this.flame.visible = false;

  //  POWER TEXT
  this.powerText = this.add.text(8,8, "Power: "+ this.power , {font: "18px Arial", fill: "#FFFFFF"});
  this.powerText.setShadow(1,1, "rgba(0, 0, 0, 0.8)", 1);
  this.powerText.fixedToCamera = true;

  //  CONTROLS
  this.cursors = this.input.keyboard.createCursorKeys();

  this.fireButton = this.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
  this.fireButton.onDown.add(this.fire, this);
  console.log("hello from GAME");
};

TanksGameWrapper.Game.prototype.fire = function (context){ 
  console.log("kaboom");

  if (this.bullet.exists) {
    return;
  }

  //  reset bullet positon
  this.bullet.reset(this.turret.x, this.turret.y);

  //  calcualte end of turret
  var p = new Phaser.Point(this.turret.x, this.turret.y);
  p.rotate(p.x, p.y, this.turret.rotation, false, 34);  

  //  position fire sprite
  this.flame.x = p.x;
  this.flame.y = p.y;
  this.flame.alpha = 1;
  this.flame.visible = true;
  this.add.tween(this.flame).to({ alpha: 0 }, 100, "Linear", true);

  this.camera.follow(this.bullet);

  this.physics.arcade.velocityFromRotation(this.turret.rotation, this.power, this.bullet.body.velocity);
};

TanksGameWrapper.Game.prototype.update = function () {
  
  // if bullet is in flight tank control is disabled
  if (this.bullet.exists) {
    if (this.bullet.y > 420) {
      this.removeBullet();
    }
    else {
      this.physics.arcade.overlap(this.bullet, this.targets, this.hitTarget, null, this);
    }
  }
  else {
    // power is set-able between 100 and 600
    if (this.cursors.left.isDown && this.power > 100 ) {
      this.power -=2;
    }
    else if (this.cursors.right.isDown && this.power < 600) {
      this.power +=2;
    }

    // angle is set-able bewtween -90 and 0
    if (this.cursors.up.isDown && this.turret.angle > -90 ) {
      this.turret.angle --;
    }
    else if (this.cursors.down.isDown && this.turret.angle < 0 ) {
      this.turret.angle ++;
    }

    // update info text
    this.powerText.text = "Power: " + this.power;  
  }

};

TanksGameWrapper.Game.prototype.hitTarget = function (bullet, target) {
  target.kill();
  this.removeBullet();
};
TanksGameWrapper.Game.prototype.removeBullet = function () {
  this.bullet.kill();
  this.camera.follow();
  this.add.tween(this.camera).to({ x: 0 }, 750, "Quint", true, 500 );
};


// TanksGameWrapper.Game.prototype.preload = function () {};