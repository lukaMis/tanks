
/* This state is to prepare all things */

var TanksGameWrapper = {};

TanksGameWrapper.Boot = function (game) {};

TanksGameWrapper.Boot.prototype.init = function () {
  

  this.game.renderer.renderSession.roundPixels = true;
  
  this.game.world.setBounds(0, 0, 992, 480);
  
  this.physics.startSystem(Phaser.Physics.ARCADE);
  
  this.physics.arcade.gravity.y = 200;

  this.stage.backgroundColor = "#FFFFFF";


};
TanksGameWrapper.Boot.prototype.preload = function () {

};
TanksGameWrapper.Boot.prototype.create = function () {
  console.log("hello from BOOT");
  game.state.start("Preload");
};